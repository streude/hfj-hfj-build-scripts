<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Development config overrides & db credentials
 * 
 * Our database credentials and any environment-specific overrides
 * 
 * @package    Focus Lab Master Config
 * @version    1.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$env_db['hostname'] = 'hfj-production.ccjmnngkuxqg.us-east-1.rds.amazonaws.com';
$env_db['username'] = 'eeuser';
$env_db['password'] = 'testEEpass';
$env_db['database'] = 'eedb';


$env_config['webmaster_email'] = 'durham@typography.com';


/* End of file config.dev.php */
/* Location: ./config/config.dev.php */