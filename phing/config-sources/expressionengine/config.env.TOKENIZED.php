<?php
/*	
H+FJ config.env.php for ExpressionEngine

DO NOT COMMIT THE LOCAL VERSION OF THIS FILE
See this file's sibling, env.orig.php for details on usage

*/

if ( ! defined('ENV')) {
	define('ENV', '##ENV##');
	define('ENV_FULL', '##ENV_FULL##');
	define('ENV_DEBUG', ##ENV_DEBUG##);
}